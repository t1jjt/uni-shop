import App from './App'
import {$http} from '@escook/request-miniprogram'
import store from '@/store/store.js'
// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'
Vue.prototype.$store = store
const app = new Vue({
    ...App,
    store,
})
app.$mount()
// #endif

// 网络请求的包
// wx.$http=$http
uni.$http=$http
// 配置请求根路径
$http.baseUrl="https://api-ugo-web.itheima.net"
$http.beforeRequest=function(options){
 uni.showLoading({
  title:"数据正在加载"
 })
   // 判断请求的是否为有权限的 API 接口
   if (options.url.indexOf('/my/') !== -1) {
     // 为请求头添加身份认证字段
     options.header = {
       // 字段的值可以直接从 vuex 中进行获取
       Authorization: store.state.m_user.token,
     }
   }
}
$http.afterRequest=function(){
 uni.hideLoading()
}
//封装数据请求信息
uni.$showMsg=function(title='数据加载失败！',duration=2000){
 uni.showToast({
  title:title,
  duration:duration,
  icon:'none'
 })
}
